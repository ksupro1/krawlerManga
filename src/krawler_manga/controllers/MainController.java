/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import krawler_manga.Krawler_Manga;

/**
 * FXML Controller class
 *
 * @author kinfinityIII
 */
public class MainController implements Initializable {
    @FXML
    Button Updates,Favourites,History,Downloads,Search,Settings,Exit;
    /**
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Krawler_Manga._stage0 = new Stage();
        Krawler_Manga._stage1 = new Stage();
        Krawler_Manga._stage2 = new Stage();
        Krawler_Manga._stage3 = new Stage();
        Krawler_Manga._stage4 = new Stage();
        Krawler_Manga._stage5 = new Stage();
        Krawler_Manga._stage6 = new Stage();
        Krawler_Manga._stage7 = new Stage();
    }    
    public void Exit(ActionEvent ev) throws IOException{
        Krawler_Manga._stage0.close();
    }
    public void Settings(ActionEvent ev) throws IOException{
        if(Krawler_Manga._stage1.isShowing()){
            Krawler_Manga._stage1.toFront();
        }else{
            Krawler_Manga._stage0.setY(Krawler_Manga.X_LEVEL_MAIN);
            Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/settings.fxml"));
            Scene sc = new Scene(root);
            Krawler_Manga._stage1.initStyle(StageStyle.UNDECORATED);
            Krawler_Manga._stage1.setScene(sc);
            Krawler_Manga._stage1.show();
        }
    }
    public void Search(ActionEvent ev) throws IOException{
        Krawler_Manga.__MODE__ = "SEARCH";
        if(Krawler_Manga._stage2.isShowing()){
            Krawler_Manga._stage2.toFront();
        }else{
            Krawler_Manga._stage0.setY(Krawler_Manga.X_LEVEL_MAIN);
            Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/search.fxml"));
            Scene sc = new Scene(root);
            Krawler_Manga._stage2.initStyle(StageStyle.UNDECORATED);
            Krawler_Manga._stage2.setScene(sc);
            Krawler_Manga._stage2.show();
            Krawler_Manga._stage2.setY(Krawler_Manga.Y_LEVEL_1);
        }
        
    }
    public void Downloads(ActionEvent ev) throws IOException{
        Krawler_Manga.__MODE__ = "DOWNLOADS";
        if(Krawler_Manga._stage3.isShowing()){
            Krawler_Manga._stage3.toFront();
        }else{
            Krawler_Manga._stage0.setY(Krawler_Manga.X_LEVEL_MAIN);
            Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/updates_favs_dls.fxml"));
            Scene sc = new Scene(root);
            Krawler_Manga._stage3.initStyle(StageStyle.UNDECORATED);
            Krawler_Manga._stage3.setScene(sc);
            Krawler_Manga._stage3.show();
            Krawler_Manga._stage3.setY(Krawler_Manga.Y_LEVEL_1);
            Krawler_Manga._stage3.setX(Krawler_Manga.X_LEVEL_1);
        }
    }
    public void History(ActionEvent ev) throws IOException{
        if(Krawler_Manga._stage4.isShowing()){
            Krawler_Manga._stage4.toFront();
        }else{
            Krawler_Manga._stage0.setY(Krawler_Manga.X_LEVEL_MAIN);
            Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/history.fxml"));
            Scene sc = new Scene(root);
            Krawler_Manga._stage4.initStyle(StageStyle.UNDECORATED);
            Krawler_Manga._stage4.setScene(sc);
            Krawler_Manga._stage4.show();
        }
    }
    public void Favourites(ActionEvent ev) throws IOException{
        Krawler_Manga.__MODE__ = "FAVOURITES";
        if(Krawler_Manga._stage5.isShowing()){
            Krawler_Manga._stage5.toFront();
        }else{
            Krawler_Manga._stage0.setY(Krawler_Manga.X_LEVEL_MAIN);
            Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/updates_favs_dls.fxml"));
            Scene sc = new Scene(root);
            Krawler_Manga._stage5.initStyle(StageStyle.UNDECORATED);
            Krawler_Manga._stage5.setScene(sc);
            Krawler_Manga._stage5.show();
            Krawler_Manga._stage5.setY(Krawler_Manga.Y_LEVEL_1);
            Krawler_Manga._stage5.setX(Krawler_Manga.X_LEVEL_1);
        }
        
    }
    public void Updates(ActionEvent ev) throws IOException{
        Krawler_Manga.__MODE__ = "UPDATES";
        if( Krawler_Manga._stage6.isShowing()){
            Krawler_Manga._stage6.toFront();
        }else{
            Krawler_Manga._stage0.setY(Krawler_Manga.Y_LEVEL_MAIN);
            Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/updates_favs_dls.fxml"));
            Scene sc = new Scene(root);
            Krawler_Manga._stage6.initStyle(StageStyle.UNDECORATED);
            Krawler_Manga._stage6.setScene(sc);
            Krawler_Manga._stage6.show();
            Krawler_Manga._stage6.setY(Krawler_Manga.Y_LEVEL_1);
            Krawler_Manga._stage6.setX(Krawler_Manga.X_LEVEL_1);
            
            
        }
    }
    
    
}
